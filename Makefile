

all: web-server hw

web-server : web-server.c
	gcc -I. web-server.c -o web-server -pthread

hw : hw.c
	gcc hw.c -o hw

clean:
	rm -f *.o web-server *~ hw
