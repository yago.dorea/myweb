/* tcp-server.c - A simple multithread web server.

   Copyright (c) 2015, Monaco F. J. <monaco@usp.br>

   This file is part of POSIX.

   POSIX is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <debug.h>

#include <stdio.h>		
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>		
#include <sys/socket.h>
#include <arpa/inet.h>          
#include <errno.h>
#include <pthread.h>

#define PORT 8080		/* A high port to avoid well-known ports. */

/* For simplicity, we will always return a static page. */

char buffer[] =
  "HTTP/1.1 200 OK\r\n"
  "Content-type: text/html\r\n"
  "\r\n"
  "<html>\r\n"
  "<head></head>\r\n"
  "<body>\r\n"
  "   <h1> Hello Web </h1>\r\n"
  "     <p>Yes, it's that easy.</p>\r\n"
  "</body>\r\n"
  "</html>\r\n";


/* This is our worker thread. */

void * serve_client (void *arg)
{
  int new_fd;

  new_fd = *(int *) arg;
  
  write (new_fd, buffer, strlen (buffer));
  
  close (new_fd);
  
  return NULL;
}

/* Create a TCP server. */

int tcp_server (int port)
{
  int fd;			
  struct sockaddr_in server_name;
  int rs;


  /* Create a socket. */
  
  fd = socket (AF_INET, SOCK_STREAM, 0);
  sysfatal (fd<0);
  
  /* Give the socket a name. */
  
  server_name.sin_family = AF_INET;                  /* Internet family */
  server_name.sin_port = htons (port);               /* Port to listen to */
  server_name.sin_addr.s_addr = htonl (INADDR_ANY);  /* Interface to listen to*/

  /* Bind socket to the service port. */
  
  rs = bind (fd, (struct sockaddr *) &server_name, sizeof (server_name));
  sysfatal (rs<0);
  
  /* Start to listen.*/

  rs = listen (fd, 1);
  sysfatal (rs<0);

  return fd;
}

/* The main server. */

int main (int argc, char **argv)
{
  int fd, new_fd;			
  struct sockaddr_in client_name;
  size_t size;
  pthread_t new_thread;


  fd = tcp_server (PORT);

  /* Accept connection request. */
  
  size = sizeof(client_name);

  while (1)
    {

      do
	new_fd = accept (fd, (struct sockaddr *) &client_name, (socklen_t*) &size);
      while ( (new_fd<0) && (errno = EINTR));
      sysfatal (new_fd <0);
      
      
      fprintf (stderr,"Client connected from host %s, port %u\n",
	       inet_ntoa (client_name.sin_addr),
	   ntohs (client_name.sin_port));

      pthread_create (&new_thread, NULL, serve_client, (int *) &new_fd);
    }


  close (fd);			/* Let's close the server's main socket. */
  
  return EXIT_FAILURE;
}
